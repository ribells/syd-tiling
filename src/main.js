//if(typeof require !== "undefined"){
//	var THREE = require( 'three' );
//}
var test = false;
var returnMessage = "";
/*
* Top level export object
*/
(function(exports){

	/*
	* The primary Tiling object. This will have all functionality required to support the current product line.
	* QT stands for Quad Tiling as these tilings all have 4 sides. This is important because the existing
	* automation supports patterns with other types of polygons, but that adds much more complexity to the automation.
	* By using the QT object we can limit this development to four sided patterns, but leave the door open to extend
	* the automation in the future with additional or alternative Tessellators.
	*/
   exports.QT = {

		/*
		* added as simple way to confirm module is loaded
		*/
		Loaded: function(){
			return "syd-tiling is loaded";
		},

		RunInTestMode: function() {
            test = true;
    	},

		/*
		* primary function that recalculates the feature based on the updated JSON data model
		*/
		UpdateFeature: function(){

			if(test == true) {
				returnMessage = "";
                var t0 = performance.now(); //use this for performance timing profiles
            }
			// select tiling type
			switch(this.Properties.UserInputs.Type){
				case 100:
					results = this.Tetria();
				break;
				case 200:
					results = this.Clario();
				break;
				case 300:
					results = this.Velo();
				break;
				case 400:
					results = this.HushBlock();
				break;
				case 500:
					results = this.Swoon();
				break;
			}
			if(test == true) {
                var t1 = performance.now(); //use this for performance timing profiles
            }
			if(test == true) {
                console.log('EXECUTION TIME:' + (performance.now() - t0)+ " milliseconds.");
				returnMessage += "syd-tile t1:" + (t1 - t0) + " milliseconds";
                return returnMessage;
            }

			if(results != "success"){
				console.log(results);
				return results; //perhaps this returns only in dev
			}
			return results
		},

		/*
		 * Tetria Tiling.
		 */
		Tetria: function() {
			this.Properties.UserInputs.Width = 24;
			this.Properties.UserInputs.Height = 24;
            this.Properties.Tiling.Boxes = new Array();
            
			for(var j=0;j<this.Properties.UserInputs.NumY;j++) {
				for(var i=0;i<this.Properties.UserInputs.NumX;i++) {
					box = new Array();
					if(this.Properties.UserInputs.Tiles[j][i].texture=='') {
						//create a off-screen box for now
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
					} else {
						box.push(new Array(
							i*this.Properties.UserInputs.Width+.34375,
							j*this.Properties.UserInputs.Height+.34375, 
							0)
						);
						box.push(new Array(
							(i+1)*this.Properties.UserInputs.Width-.34375,
							j*this.Properties.UserInputs.Height+.34375, 
							0)
						);					
						box.push(new Array(
							(i+1)*this.Properties.UserInputs.Width-.34375,
							(j+1)*this.Properties.UserInputs.Height-.34375, 
							0)
						);
						box.push(new Array(
							i*this.Properties.UserInputs.Width+.34375,
							(j+1)*this.Properties.UserInputs.Height-.34375, 
							0)
						);
						box.push(new Array(
							i*this.Properties.UserInputs.Width+.34375,
							j*this.Properties.UserInputs.Height+.34375, 
							.133)
						);
						box.push(new Array(
							(i+1)*this.Properties.UserInputs.Width-.34375,
							j*this.Properties.UserInputs.Height+.34375, 
							.133)
						);					
						box.push(new Array(
							(i+1)*this.Properties.UserInputs.Width-.34375,
							(j+1)*this.Properties.UserInputs.Height-.34375, 
							.133)
						);					
						box.push(new Array(
							i*this.Properties.UserInputs.Width+.34375,
							(j+1)*this.Properties.UserInputs.Height-.34375, 
							.133)
						);
						//set the type and orientation of the tile
						if(this.Properties.UserInputs.Tiles[j][i].tile=='01' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==0) {
							box[4][2] = 3.0;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='01' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==90) {
							box[5][2] = 3.0;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='01' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==180) {
							box[6][2] = 3.0;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='01' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==270) {
							box[7][2] = 3.0;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='02' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==0) {
							box[4][2] = 3.0;
							box[5][2] = 3.0;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='02' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==90) {
							box[5][2] = 3.0;
							box[6][2] = 3.0;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='02' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==180) {
							box[6][2] = 3.0;
							box[7][2] = 3.0;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='02' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==270) {
							box[7][2] = 3.0;
							box[4][2] = 3.0;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='03' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==0) {
							box[4][2] = 3.0;
							box[6][2] = 2.84;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='03' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==90) {
							box[5][2] = 3.0;
							box[7][2] = 2.84;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='03' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==180) {
							box[6][2] = 3.0;
							box[4][2] = 2.84;
						} else if(this.Properties.UserInputs.Tiles[j][i].tile=='03' &&
						   this.Properties.UserInputs.Tiles[j][i].rotation==270) {
							box[7][2] = 3.0;
							box[5][2] = 2.84;
						}
					}
					this.Properties.Tiling.Boxes.push(box);
				}
			}
			//alert(this.Properties.Tiling.Boxes);

            return "success";
		},

		/*
		 * Clario Tiling
		 */
		Clario: function() {
			this.Properties.UserInputs.Width = 24;
			this.Properties.UserInputs.Height = 24;
            this.Properties.Tiling.Boxes = new Array();
            
			for(var j=0;j<this.Properties.UserInputs.NumY;j++) {
				for(var i=0;i<this.Properties.UserInputs.NumX;i++) {
					box = new Array();
					if(this.Properties.UserInputs.Tiles[j][i].texture=='') {
						//create a off-screen box for now
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
					} else if(this.Properties.UserInputs.Tiles[j][i].tile=='24' || 
							  this.Properties.UserInputs.Tiles[j][i].tile=='48') {
						//create a off-screen boxes for now with tile and rotation values passed in
						box.push(new Array(this.Properties.UserInputs.Tiles[j][i].tile,
						 				  -10000.0,
						                   this.Properties.UserInputs.Tiles[j][i].rotation
						                  ));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
						box.push(new Array(-10000.0, -10000.0, -10000.0));
					} else {
						box.push(new Array(
							i*this.Properties.UserInputs.Width+.34375,
							j*this.Properties.UserInputs.Height+.34375, 
							0)
						);
						box.push(new Array(
							(i+1)*this.Properties.UserInputs.Width-.34375,
							j*this.Properties.UserInputs.Height+.34375, 
							0)
						);					
						box.push(new Array(
							(i+1)*this.Properties.UserInputs.Width-.34375,
							(j+1)*this.Properties.UserInputs.Height-.34375, 
							0)
						);
						box.push(new Array(
							i*this.Properties.UserInputs.Width+.34375,
							(j+1)*this.Properties.UserInputs.Height-.34375, 
							0)
						);
						box.push(new Array(
							i*this.Properties.UserInputs.Width+.34375,
							j*this.Properties.UserInputs.Height+.34375, 
							.133)
						);
						box.push(new Array(
							(i+1)*this.Properties.UserInputs.Width-.34375,
							j*this.Properties.UserInputs.Height+.34375, 
							.133)
						);					
						box.push(new Array(
							(i+1)*this.Properties.UserInputs.Width-.34375,
							(j+1)*this.Properties.UserInputs.Height-.34375, 
							.133)
						);					
						box.push(new Array(
							i*this.Properties.UserInputs.Width+.34375,
							(j+1)*this.Properties.UserInputs.Height-.34375, 
							.133)
						);
					}
					this.Properties.Tiling.Boxes.push(box);
				}
			}

			return "success";
		},

		/*
		 * Velo Tiling
		 */
		Velo: function() {
			var is_concave = 0;
            this.Properties.Tiling.Boxes = new Array();
            //parse the tiles
			for(var i=0;i<this.Properties.UserInputs.Tiles.length;i++) {
            	if(this.Properties.UserInputs.Tiles[i].tile == "concave") {
            		is_concave = 1;
            	} else {
            		is_concave = 0;
            	}
				box = new Array();
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].pentagon[0][0],
					this.Properties.UserInputs.Tiles[i].pentagon[0][1], 
					is_concave)
				);
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].pentagon[1][0],
					this.Properties.UserInputs.Tiles[i].pentagon[1][1], 
					is_concave)
				);					
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].pentagon[2][0],
					this.Properties.UserInputs.Tiles[i].pentagon[2][1], 
					is_concave)
				);
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].pentagon[3][0],
					this.Properties.UserInputs.Tiles[i].pentagon[3][1], 
					is_concave)
				);
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].pentagon[4][0],
					this.Properties.UserInputs.Tiles[i].pentagon[4][1], 
					is_concave)
				);
				box.push(new Array(
					0,
					0, 
					0)
				);					
				box.push(new Array(
					0,
					0, 
					0)
				);					
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].row,
					this.Properties.UserInputs.Tiles[i].column, 
					this.Properties.UserInputs.Tiles[i].rotation)
				);
				this.Properties.Tiling.Boxes.push(box);
			}

            return "success";
		},

		/*
		 * HushBlock Tiling
		 */
		HushBlock: function() {
            this.Properties.Tiling.Boxes = new Array();
            //parse the tiles
			for(var i=0;i<this.Properties.UserInputs.Tiles.length;i++) {
				box = new Array();
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].column * 24.0,
					this.Properties.UserInputs.Tiles[i].row * 24.0, 
					2.0)
				);
				box.push(new Array(
					(this.Properties.UserInputs.Tiles[i].column + 1) * 24.0,
					this.Properties.UserInputs.Tiles[i].row * 24.0, 
					2.0)
				);					
				box.push(new Array(
					(this.Properties.UserInputs.Tiles[i].column + 1) * 24.0,
					(this.Properties.UserInputs.Tiles[i].row + 1) * 24.0, 
					2.0)
				);
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].column * 24.0,
					(this.Properties.UserInputs.Tiles[i].row + 1) * 24.0, 
					2.0)
				);
				box.push(new Array(
					0,
					0, 
					0)
				);
				box.push(new Array(
					0,
					0, 
					0)
				);
				box.push(new Array(
					this.Properties.UserInputs.NumX,
					this.Properties.UserInputs.NumY, 
					this.Properties.UserInputs.Tiles[i].rotation)
				);					
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].column,
					this.Properties.UserInputs.Tiles[i].row, 
					this.Properties.UserInputs.Tiles[i].rotation)
				);
				this.Properties.Tiling.Boxes.push(box);
			}

            return "success";
		},
		
		/*
		 * Swoon Tiling
		 */
		Swoon: function() {    //NOTE: convert config tool pixels to inches
            this.Properties.Tiling.Boxes = new Array();
            //parse the tiles
			for(var i=0;i<this.Properties.UserInputs.Tiles.length;i++) {
				box = new Array();
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].diamond[0][0]/6.1893,
					this.Properties.UserInputs.Tiles[i].diamond[0][1]/6.1893, 
					2.0)
				);
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].diamond[1][0]/6.1893,
					this.Properties.UserInputs.Tiles[i].diamond[1][1]/6.1893,
					2.0)
				);					
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].diamond[2][0]/6.1893,
					this.Properties.UserInputs.Tiles[i].diamond[2][1]/6.1893,
					2.0)
				);
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].diamond[3][0]/6.1893,
					this.Properties.UserInputs.Tiles[i].diamond[3][1]/6.1893, 
					2.0)
				);
				box.push(new Array(
					0,
					0, 
					0)
				);
				box.push(new Array(
					0,
					0, 
					0)
				);
				box.push(new Array(
					0,
					0, 
					this.Properties.UserInputs.Tiles[i].rotation)
				);					
				box.push(new Array(
					this.Properties.UserInputs.Tiles[i].column,
					this.Properties.UserInputs.Tiles[i].row, 
					this.Properties.UserInputs.Tiles[i].rotation)
				);
				this.Properties.Tiling.Boxes.push(box);
			}

            return "success";
		},

		/*
		* Properties required to run tessellation are currently in this Properties object.
		* In the future, these could get pulled from an outside JSON model or perhaps this object 
		* could be moved outside of QT and becomes the basis for a broader QT object.
		*/
		Properties: {
			Tiling: {
				Boxes: []
			},
			// This object stores the user inputs, the specification for the tessellation.  This would be JSON object with preset default properties.  Those properties would be overridden by the user inputs each time the tessellation is recalculated.
			UserInputs: {
				//0 = straight partition, 1 = arc partition, 2 = bent partition, 3 = facing, 4 = transition, 5 = ceiling
				Type: 0,
				//in decimal inches
				Width: 24,
				//in decimal inches
				Height: 24,
				//in decimal inches
				NumX: 2,
				//in decimal degrees (or radians?)
				NumY: 2,
				//in decimal inches
				Depth: 4,
				//in decimal inches
				Tiles: ""
			},
		},

		/*
		 * Set the user data properties via a measurements key-value data pair object
		 */
		SetUserDataProperties: function(measurements){
            var obj = JSON.stringify(measurements);
            var properties = JSON.parse(obj);
            for (var i=0; i<properties.length; i++) {
                switch(properties[i].label) {
                    case "Type":
                        this.Properties.UserInputs.Type = parseInt(properties[i].value);
                        break;
                    case "Width":
                        this.Properties.UserInputs.Width =  parseInt(properties[i].value);
                        break;
                    case "Height":
                        this.Properties.UserInputs.Height =  parseInt(properties[i].value);
                    case "NumX":
                        this.Properties.UserInputs.NumX = parseInt(properties[i].value);
                        break;
                    case "NumY":
                        this.Properties.UserInputs.NumY = parseInt(properties[i].value);
                        break;
                        break;
                    case "Depth":
                        this.Properties.UserInputs.Depth =  parseInt(properties[i].value);
                        break;
                    case "Tiles":
                        this.Properties.UserInputs.Tiles =  properties[i].value;
                        break;
                    default:
                        console.log("No such user data property: " + properties[i].label);
                }
            }
		},

		/*
		 * Set the user data properties via any JSON string that includes them
		 */
		SetUserDataPropertiesJSONString: function(jsonStr){
			var obj = JSON.parse(jsonStr);
			//console.log("Got JSON String: " + jsonStr);
			this.Properties.UserInputs.Type = parseInt(obj.UserInputs.Type);
			this.Properties.UserInputs.Width = parseInt(obj.UserInputs.Width);
			this.Properties.UserInputs.Height = parseInt(obj.UserInputs.Height);
			this.Properties.UserInputs.NumX = parseInt(obj.UserInputs.NumX);
			this.Properties.UserInputs.NumY = parseInt(obj.UserInputs.NumY);
			this.Properties.UserInputs.Depth = parseInt(obj.UserInputs.Depth);
			this.Properties.UserInputs.Tiles = obj.UserInputs.Tiles;
		},

		/*
		 * Get all the properties (in the recommended manner)
		 */
		GetAllProperties: function(){
			return this.GetAllPropertiesAsJSONString();
		},

		/*
		 * Get all the properties as encoded in a JSON string
		 */
		GetAllPropertiesAsJSONString: function(){
			var jsonStr = JSON.stringify(this.Properties);
			return jsonStr;
		},
		
		/*
		 * Get all the tiles created as boxes for visualization
		 */
		GetBoxes: function() {
			return this.Properties.Tiling.Boxes;
		},		
	}
})(typeof exports === 'undefined'? this['Syd']={}: exports);

function distance_between_points(p1, p2) {
	return Math.sqrt(((p2[0]-p1[0]) * (p2[0]-p1[0])) + ((p2[1]-p1[1]) * (p2[1]-p1[1])) + ((p2[2]-p1[2]) * (p2[2]-p1[2])));
}

function cross(a, b) {
	c = [0,0,0];
	c[0] = a[1] * b[2] - a[2] * b[1];
	c[1] = a[2] * b[0] - a[0] * b[2];
	c[2] = a[0] * b[1] - a[1] * b[0];
	return c;
}

function normalize(v) {
	length = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	v[0] = v[0]/length;
	v[1] = v[1]/length;
	v[2] = v[2]/length;
	return v;
}